package Modul_3A;

import javax.swing.*;


public class FrameKu_Latihan3 extends JFrame {
    
    public FrameKu_Latihan3(){
     this.setSize(300,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel=new JPanel();
        JButton tombol = new JButton();
        tombol.setText("ini Tombol");
        panel.add(tombol);
        this.add(panel);

        JLabel label=new JLabel("labelku");
        panel.add(label);
        this.add(panel);
        
        JTextField text= new JTextField("Text Field");
        panel.add (text);
        this.add (panel);
        
        JCheckBox box= new JCheckBox("Check Box");
        panel.add (box);
        this.add (panel);
        
        JRadioButton radio= new  JRadioButton("Radio Button");
        panel.add (radio);
        this.add (panel);
    }
    public static void main(String[] args) {
        new FrameKu_Latihan3();
    }
}
    

