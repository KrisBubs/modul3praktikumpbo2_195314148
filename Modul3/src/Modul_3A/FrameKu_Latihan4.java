package Modul_3A;
        
import javax.swing.*;
        
public class FrameKu_Latihan4 extends JFrame{
    
public FrameKu_Latihan4(){
        this.setLayout(null);
        this.setSize(400,150);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);

        JLabel label= new JLabel("Keyboard");
        label.setBounds(150,20,100,20);
        this.add(label);


        JTextField text = new JTextField();
        text.setBounds(150,50,150,20);
        this.add(text);
        
        JButton tombol = new JButton();
        tombol.setText("Find");
        tombol.setBounds(150,70,100,20);
        this.add(tombol);
        
    }
    public static void main(String[] args) {
        new FrameKu_Latihan4();
    }
}
